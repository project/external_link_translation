FROM drupal:10-apache

ENV COMPOSER_ALLOW_SUPERUSER 1

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && { \
        echo 'xdebug.mode=off'; \
        echo 'xdebug.start_with_request=yes'; \
        echo 'xdebug.discover_client_host=0'; \
        echo 'xdebug.client_host=host.docker.internal'; \
    } >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN composer require drush/drush:^12 \
                     drupal/core-dev:^10 --dev --with-all-dependencies

RUN chown -R www-data:www-data /opt/drupal

USER www-data
