<?php

/**
 * @file
 * Builds placeholder replacement tokens for external link translations.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function external_link_translation_token_info() {
  $types['external_link_translation'] = [
    'name' => t('External link translation'),
    'description' => t('Tokens related to external link translations.'),
    'needs-data' => 'external_link_translation',
  ];

  $elt['created'] = [
    'name' => t('Created'),
    'description' => t('The time that the external link translation was created.'),
    'type' => 'date',
  ];
  $elt['id'] = [
    'name' => t('ID'),
    'description' => t('The unique ID of the external link translation.'),
  ];
  $elt['link'] = [
    'name' => t('Link'),
    'description' => t('The external link translation URL.'),
  ];

  return [
    'types' => $types,
    'tokens' => ['external_link_translation' => $elt],
  ];
}

/**
 * Implements hook_tokens().
 */
function external_link_translation_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  $langcode = $options['langcode'] ?? NULL;

  if ($type == 'external_link_translation' && !empty($data['external_link_translation'])) {
    /** @var \Drupal\external_link_translation\ExternalLinkTranslationInterface $elt */
    $elt = $data['external_link_translation'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'created':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = $elt->getCreatedTime() ? \Drupal::service('date.formatter')->format($elt->getCreatedTime(), 'medium', '', NULL, $langcode) : t('not yet created');
          break;

        case 'id':
          $replacements[$original] = $elt->id() ?: t('not yet assigned');
          break;

        case 'link':
          $replacements[$original] = $elt->getLink() ?: t('not yet created');
          break;

        case 'changed':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = $elt->getChangedTime() ? \Drupal::service('date.formatter')->format($elt->getChangedTime(), 'medium', '', NULL, $langcode) : t('not yet changed');
          break;
      }
    }
  }

  return $replacements;
}
