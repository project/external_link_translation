/**
 * @file
 * Provides helpers for administrating external link translations.
 */

(function externalLinkTranslationAdmin(Drupal) {
  Drupal.behaviors.externalLinkTranslationCopy = {
    attach(context, settings) {
      const elements = once('elt-copy', '.elt-copy', context);
      elements.forEach(el => el.addEventListener('click', clipBoardCopy, false));

      /**
       * Copies the link to the clipboard.
       *
       * @param {Event} event - The event that triggers the function
       */
      function clipBoardCopy(event) {
        const currentElement = event.target;
        if (!currentElement.hasAttribute('data-copy-link')) {
          return;
        }

        const text = document.createElement('input');
        text.setAttribute('type', 'text');
        text.setAttribute('value', currentElement.getAttribute('data-copy-link'));
        text.setAttribute('class', 'hidden');

        document.body.appendChild(text);
        text.select();
        text.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(text.value).then(() => {
          const originalText = currentElement.innerText;
          currentElement.innerText = Drupal.t('Copied');
          setTimeout(() => {
            currentElement.innerText = originalText;
          }, 1000);
        });
        document.body.removeChild(text);

        event.preventDefault();
        event.stopPropagation();
      }
    }
  };
})(Drupal);
