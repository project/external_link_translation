<?php

declare(strict_types=1);

namespace Drupal\external_link_translation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\Core\Url;
use Drupal\external_link_translation\ExternalLinkTranslationInterface;
use Drupal\link\LinkItemInterface;

/**
 * Defines the external link translation entity class.
 *
 * @ContentEntityType(
 *   id = "external_link_translation",
 *   label = @Translation("External link translation"),
 *   label_collection = @Translation("External link translations"),
 *   label_singular = @Translation("External link translation"),
 *   label_plural = @Translation("External link translations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count external link translation",
 *     plural = "@count external link translations",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\external_link_translation\ExternalLinkTranslationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\external_link_translation\Form\ExternalLinkTranslationForm",
 *       "edit" = "Drupal\external_link_translation\Form\ExternalLinkTranslationForm",
 *       "delete" = "Drupal\external_link_translation\Form\ExternalLinkTranslationDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "external_link_translation",
 *   data_table = "external_link_translation_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer external link translations",
 *   collection_permission = "access external link translations overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/external-link-translation/{external_link_translation}",
 *     "collection" = "/admin/content/external-link-translation",
 *     "add-form" = "/external-link-translation/add",
 *     "edit-form" = "/external-link-translation/{external_link_translation}/edit",
 *     "delete-form" = "/external-link-translation/{external_link_translation}/delete",
 *   },
 * )
 */
final class ExternalLinkTranslation extends ContentEntityBase implements ExternalLinkTranslationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['link'] = BaseFieldDefinition::create('link')
      ->setLabel(new TranslatableMarkup('External link'))
      ->setRequired(TRUE)
      ->setRevisionable(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => -1,
      ])
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_EXTERNAL,
        'title' => DRUPAL_DISABLED,
      ])
      ->addPropertyConstraints('uri', ['EltUrl' => []]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the external link translation was created.'))
      ->setTranslatable(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the external link translation was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getLink();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): ?int {
    $value = $this->get('created')->value;
    return isset($value) ? (int) $value : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLink(): string {
    return $this->getUrlObject()->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function setLink(string $link): static {
    $this->set('link', ['uri' => $link]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlObject(): Url {
    try {
      return $this->get('link')->first()->getUrl();
    }
    catch (MissingDataException | \InvalidArgumentException $e) {
      // Default to the home page as a fail-safe to prevent error propagation.
      return Url::fromRoute('<front>');
    }
  }

}
