<?php

declare(strict_types=1);

namespace Drupal\external_link_translation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Url;

/**
 * Provides an interface defining an external link translation entity type.
 */
interface ExternalLinkTranslationInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the external link translation entity creation timestamp.
   *
   * @return int|null
   *   The creation timestamp, or NULL if unknown.
   */
  public function getCreatedTime(): ?int;

  /**
   * Gets the external link URL.
   *
   * @return string
   *   The URL of the external link.
   */
  public function getLink(): string;

  /**
   * Sets the external link URL.
   *
   * @param string $link
   *   The URL to set for the external link.
   *
   * @return $this
   *   The instance of the current entity, for method chaining.
   */
  public function setLink(string $link): static;

  /**
   * Gets the external link URL object.
   *
   * @return \Drupal\Core\Url
   *   An Url object instance.
   */
  public function getUrlObject(): Url;

}
