<?php

declare(strict_types=1);

namespace Drupal\external_link_translation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;

/**
 * Provides a list controller for the external link translation entity type.
 */
final class ExternalLinkTranslationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [
      'link' => $this->t('External Link'),
      'copy_link' => $this->t('Copy & Use Link'),
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\external_link_translation\ExternalLinkTranslationInterface $entity */
    $row['link'] = $entity->toLink();
    $row['copy_link'] = '/external-link-translation/' . $entity->id();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritDoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations['copy'] = [
      'title' => $this->t('Copy'),
      'url' => Url::fromUserInput('#'),
      'attributes' => [
        'data-copy-link' => '/external-link-translation/' . $entity->id(),
        'class' => ['elt-copy'],
      ],
      'weight' => 0,
    ];
    return $operations + parent::getDefaultOperations($entity);
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    $build = parent::render();
    $build['#attached']['library'][] = 'external_link_translation/admin';
    return $build;
  }

}
