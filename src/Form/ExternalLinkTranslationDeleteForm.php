<?php

declare(strict_types=1);

namespace Drupal\external_link_translation\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the external link translation entity delete forms.
 */
class ExternalLinkTranslationDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    // Redirect to the collection page.
    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

}
