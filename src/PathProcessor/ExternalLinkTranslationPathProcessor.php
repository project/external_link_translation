<?php

namespace Drupal\external_link_translation\PathProcessor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor for external link translations.
 */
class ExternalLinkTranslationPathProcessor implements OutboundPathProcessorInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs an ExternalLinkTranslationPathProcessor object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL): string {
    /** @var \Symfony\Component\Routing\Route $route */
    $route = $options['route'] ?? NULL;
    if (empty($route) || $route->getPath() !== '/external-link-translation/{external_link_translation}') {
      return $path;
    }

    $langcode = isset($options['language']) ? $options['language']->getId() : $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_URL)->getId();
    $parts = explode('/', $path);
    $id = end($parts);
    $link = $this->getExternalLinkFromId($id, $langcode);
    if ($link && filter_var($link, FILTER_VALIDATE_URL)) {
      $parsed_link = parse_url($link);
      $options['prefix'] = '';
      $options['absolute'] = TRUE;
      $options['base_url'] = $parsed_link['scheme'] . '://' . $parsed_link['host'];
      $options['fragment'] = $parsed_link['fragment'] ?? '';
      $options['query'] = [];
      parse_str($parsed_link['query'] ?? '', $options['query']);
      $path = $parsed_link['path'] ?? '/';
      // Add cache tag for entity.
      $bubbleable_metadata?->addCacheTags(['external_link_translation:' . $id]);
    }

    return $path;
  }

  /**
   * Retrieves the translated external link based on the given ID.
   *
   * @param string $id
   *   The ID of the external link translation entity.
   * @param string $langcode
   *   The language code.
   *
   * @return string|null
   *   The translated external link if available, or NULL otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getExternalLinkFromId(string $id, string $langcode): ?string {
    /** @var \Drupal\external_link_translation\ExternalLinkTranslationInterface $entity */
    $entity = $this->entityTypeManager->getStorage('external_link_translation')->load($id);
    if ($entity !== NULL) {
      $translated_entity = $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : $entity;
      return $translated_entity->getLink();
    }

    return NULL;
  }

}
