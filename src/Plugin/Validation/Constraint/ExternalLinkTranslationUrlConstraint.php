<?php

namespace Drupal\external_link_translation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraints\Url;

/**
 * Validates that a value is a valid URL for external link translations.
 *
 * @Constraint(
 *   id = "EltUrl",
 *   label = @Translation("External Link Translation Url", context = "Validation")
 * )
 */
class ExternalLinkTranslationUrlConstraint extends Url {

  /**
   * Error message for constraint.
   *
   * @var string
   */
  public $message = 'The URL %value is invalid. Please check for any typos or missing parts and try again.';

  /**
   * Allowed protocols.
   *
   * @var array
   */
  public $protocols = ['http', 'https'];

  /**
   * {@inheritdoc}
   */
  public function validatedBy(): string {
    return '\Symfony\Component\Validator\Constraints\UrlValidator';
  }

}
