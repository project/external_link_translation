<?php

namespace Drupal\Tests\external_link_translation\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test.
 *
 * @group external_link_translation
 */
class ExternalLinkTranslationGenericTest extends GenericModuleTestBase {}
