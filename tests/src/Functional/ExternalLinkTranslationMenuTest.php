<?php

namespace Drupal\Tests\external_link_translation\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Tests the use of an external link translation in menus.
 *
 * @group external_link_translation
 */
class ExternalLinkTranslationMenuTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'external_link_translation',
    'link',
    'content_translation',
    'block',
    'menu_link_content',
    'language',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('system_menu_block:main');

    // Configure language settings.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    ConfigurableLanguage::createFromLangcode('de')->save();
    ConfigurableLanguage::createFromLangcode('zh-hans')->save();
    ContentLanguageSettings::loadByEntityTypeBundle('external_link_translation', 'external_link_translation')
      ->setDefaultLangcode(LanguageInterface::LANGCODE_SITE_DEFAULT)
      ->setLanguageAlterable(TRUE)
      ->save();

    // Create an external link translation.
    $link = $this->container->get('entity_type.manager')->getStorage('external_link_translation')
      ->create(['link' => ['uri' => 'https://localize.drupal.org?lang=test#test']]);
    $link->save();
    $link->addTranslation('fr', ['link' => ['uri' => 'https://localize.drupal.org/fr']]);
    $link->save();
    $link->addTranslation('de', ['link' => ['uri' => 'https://localize.drupal.org/#de']]);
    $link->save();

    // Add external link translation as a main menu item.
    $menu_link = MenuLinkContent::create([
      'title' => 'Example Link',
      'menu_name' => 'main',
      'bundle' => 'menu_link_content',
      'link' => ['uri' => 'internal:/external-link-translation/1'],
    ]);
    $menu_link->save();
  }

  /**
   * Tests that menu links render external link translations.
   */
  public function testExternalLinkTranslationsInMenu() {
    $account = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($account);

    // Test default language.
    $this->drupalGet('<front>');
    $link = $this->getSession()->getPage()->find('xpath', '//a[@href="https://localize.drupal.org/?lang=test#test"]');
    $this->assertNotEmpty($link, 'Cannot find the source language external link');

    // Test FR translation.
    $this->drupalGet('fr');
    $link = $this->getSession()->getPage()->find('xpath', '//a[@href="https://localize.drupal.org/fr"]');
    $this->assertNotEmpty($link, 'Cannot find the FR external link');

    // Test DE translation.
    $this->drupalGet('de');
    $link = $this->getSession()->getPage()->find('xpath', '//a[@href="https://localize.drupal.org/#de"]');
    $this->assertNotEmpty($link, 'Cannot find the DE external link');

    // Test ZH-HANS translation (should fall back to the default).
    $this->drupalGet('zh-hans');
    $link = $this->getSession()->getPage()->find('xpath', '//a[@href="https://localize.drupal.org/?lang=test#test"]');
    $this->assertNotEmpty($link, 'Cannot find the ZH-HANS external link');
  }

}
