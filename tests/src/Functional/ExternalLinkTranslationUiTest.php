<?php

namespace Drupal\Tests\external_link_translation\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the external link translation entity UI functionality.
 *
 * @group external_link_translation
 */
class ExternalLinkTranslationUiTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['external_link_translation', 'link', 'content_translation', 'language'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an external link translation.
    $link = $this->container->get('entity_type.manager')->getStorage('external_link_translation')
      ->create(['link' => ['uri' => 'https://drupal.org/']]);
    $link->save();
  }

  /**
   * Tests that the collection page works.
   */
  public function testCollectionPage() {
    $account = $this->drupalCreateUser(['access external link translations overview']);
    $this->drupalLogin($account);

    $this->drupalGet('admin/content/external-link-translation');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('https://drupal.org/');
    $this->assertSession()->pageTextContains('/external-link-translation/1');
    $this->assertSession()->pageTextContains('Copy');
  }

  /**
   * Tests that the add page works.
   */
  public function testAddPage() {
    $account = $this->drupalCreateUser(['administer external link translations']);
    $this->drupalLogin($account);

    $this->drupalGet('external-link-translation/add');
    $this->assertSession()->statusCodeEquals(200);

    $this->getSession()->getPage()->fillField('External link', 'https://example.com/');
    $this->getSession()->getPage()->pressButton('Save');
    $this->assertSession()->pageTextContains('has been created');
  }

  /**
   * Tests that the edit page works.
   */
  public function testEditPage() {
    $account = $this->drupalCreateUser(['administer external link translations']);
    $this->drupalLogin($account);

    $this->drupalGet('external-link-translation/1/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->getSession()->getPage()->fillField('External link', 'https://example.com/');
    $this->getSession()->getPage()->pressButton('Save');
    $this->assertSession()->pageTextContains('has been updated');
  }

  /**
   * Tests that the delete page works.
   */
  public function testDeletePage() {
    $account = $this->drupalCreateUser(['administer external link translations']);
    $this->drupalLogin($account);

    $this->drupalGet('external-link-translation/1/delete');
    $this->assertSession()->statusCodeEquals(200);

    $this->getSession()->getPage()->pressButton('Delete');
    $this->assertSession()->pageTextContains('has been deleted');
  }

  /**
   * Tests access to content translation operations for users.
   */
  public function testTranslationOperations() {
    $account = $this->drupalCreateUser(['administer external link translations', 'translate editable entities']);
    $this->drupalLogin($account);

    // Setup language and content translation.
    ConfigurableLanguage::createFromLangcode('de')->save();
    \Drupal::service('content_translation.manager')->setEnabled('external_link_translation', 'external_link_translation', TRUE);
    $this->rebuildContainer();

    // Verify viewing translations is accessible.
    $this->drupalGet('external-link-translation/1/translations');
    $this->assertSession()->statusCodeEquals(200);

    // Verify creating a translation works as expected.
    $this->drupalGet('/external-link-translation/1/translations/add/en/de');
    $this->assertSession()->statusCodeEquals(200);
    $this->getSession()->getPage()->fillField('External link', 'https://example.com/');
    $this->getSession()->getPage()->pressButton('Save');

    // Verify editing a translation is accessible.
    $this->drupalGet('de/external-link-translation/1/edit');
    $this->assertSession()->statusCodeEquals(200);

    // Verify deletion of a translation is accessible.
    $this->drupalGet('de/external-link-translation/1/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

}
