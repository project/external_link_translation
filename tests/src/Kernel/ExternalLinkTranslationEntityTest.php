<?php

namespace Drupal\Tests\external_link_translation\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\external_link_translation\ExternalLinkTranslationInterface;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;

/**
 * Tests the external link translation (ELT) entity CRUD functionality.
 *
 * @coversDefaultClass \Drupal\external_link_translation\Entity\ExternalLinkTranslation
 *
 * @group external_link_translation
 */
class ExternalLinkTranslationEntityTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['external_link_translation', 'language', 'link', 'content_translation'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['language']);
    $this->installEntitySchema('external_link_translation');

    ConfigurableLanguage::createFromLangcode('de')->save();
    ContentLanguageSettings::loadByEntityTypeBundle('external_link_translation', 'external_link_translation')
      ->setDefaultLangcode(LanguageInterface::LANGCODE_SITE_DEFAULT)
      ->setLanguageAlterable(TRUE)
      ->save();
  }

  /**
   * Data provider of invalid external links to test.
   */
  public static function invalidLinkProvider(): array {
    return [
      'No protocol, TLD, etc'  => ['invalid-url'],
      'Relative internal link' => ['/node/1'],
      'External link with space at start' => [' https://drupal.org'],
      'External link with space at end'  => ['https://drupal.org '],
      'Missing scheme' => ['www.example.com'],
      'Invalid character in path' => ['http://example.com/some path'],
      'JavaScript scheme' => ['javascript:alert(1)'],
      'Data URI scheme' => ['data:text/plain;base64,SGVsbG8sIFdvcmxkIQ=='],
      'FTP scheme' => ['ftp://example.com'],
      'Mailto scheme' => ['mailto:example@test.com'],
      'Tel scheme' => ['tel:123-4567'],
      'Scheme relative URL' => ['//example.com'],
      'IP address without protocol' => ['192.168.1.1'],
      'Port without host' => [':8080'],
      'Fragment only' => ['#fragment'],
      'Query string only' => ['?key=value'],
    ];
  }

  /**
   * Tests the creation of an ELT entity with a valid external link.
   */
  public function testCreateEntity() {
    $entity = $this->createExternalLinkTranslation('https://drupal.org');
    $this->assertEquals('https://drupal.org', $entity->getLink());
    $this->assertTrue((time() - $entity->getCreatedTime()) < 10, 'Expected created timestamp to be within 10 seconds of the current time.');
  }

  /**
   * Tests the creation of an ELT entity with an invalid external link.
   *
   * @param string $invalid_link
   *   An invalid URL.
   *
   * @dataProvider invalidLinkProvider
   */
  public function testCreateEntityWithInvalidUrl(string $invalid_link) {
    $entity = $this->createExternalLinkTranslation($invalid_link, FALSE);

    // Check for validation errors.
    $violations = $entity->validate();
    $this->assertGreaterThan(0, $violations->count(), 'No validation errors found.');

    // Check if there is a violation for the link field.
    $url_violations = $violations->getByField('link');
    $this->assertGreaterThan(0, $url_violations->count(), 'No validation errors found for link field.');
  }

  /**
   * Tests the creation of an ELT entity with a different language.
   */
  public function testEntityTranslation() {
    $entity = $this->createExternalLinkTranslation('https://www.ibm.com/us-en');

    // Add a German translation.
    $entity->addTranslation('de', ['link' => ['uri' => 'https://www.ibm.com/de-de']])->save();

    // Assert that the German translation exists.
    $german_translation = $entity->getTranslation('de');
    $this->assertTrue($entity->hasTranslation('de'), 'German translation exists.');
    $this->assertEquals('https://www.ibm.com/de-de', $german_translation->getLink());
  }

  /**
   * Tests the reading of an ELT entity.
   */
  public function testReadEntity() {
    $entity = $this->createExternalLinkTranslation('https://google.com');
    $loaded_entity = $this->entityTypeManager->getStorage('external_link_translation')->load($entity->id());
    $this->assertEquals('https://google.com', $loaded_entity->getLink());
  }

  /**
   * Tests updating an existing ELT entity.
   */
  public function testUpdateEntity() {
    $entity = $this->createExternalLinkTranslation('https://docs.phpunit.de/');
    $entity->setLink('https://docs.phpunit.com/');
    $entity->save();

    $updated_entity = $this->entityTypeManager->getStorage('external_link_translation')->load($entity->id());
    $this->assertEquals('https://docs.phpunit.com/', $updated_entity->getLink());
    $this->assertTrue((time() - $entity->getChangedTime()) < 10, 'Expected changed timestamp to be within 10 seconds of the current time.');
  }

  /**
   * Tests the deletion of an ELT entity.
   */
  public function testDeleteEntity() {
    $entity = $this->createExternalLinkTranslation('https://www.hidglobal.com');
    $entity_id = $entity->id();
    $entity->delete();

    $deleted_entity = $this->entityTypeManager->getStorage('external_link_translation')->load($entity_id);
    $this->assertNull($deleted_entity);
  }

  /**
   * Helper method to create an ELT entity.
   *
   * @param string $uri
   *   The URI to set.
   * @param bool $save
   *   Whether to save after creation.
   *
   * @return \Drupal\external_link_translation\ExternalLinkTranslationInterface
   *   The created entity.
   */
  protected function createExternalLinkTranslation(string $uri, bool $save = TRUE): ExternalLinkTranslationInterface {
    $entity = $this->entityTypeManager->getStorage('external_link_translation')->create([
      'link' => ['uri' => $uri],
    ]);
    if ($save) {
      $entity->save();
    }
    return $entity;
  }

}
