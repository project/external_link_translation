<?php

namespace Drupal\Tests\external_link_translation\Kernel;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Utility\Token;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\external_link_translation\Entity\ExternalLinkTranslation;

/**
 * Tests the replacement of external link translation tokens.
 *
 * @group external_link_translation
 */
class ExternalLinkTranslationTokenTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'external_link_translation',
    'language',
    'link',
    'content_translation',
    'system',
  ];

  /**
   * An external link translation test entity.
   *
   * @var \Drupal\external_link_translation\Entity\ExternalLinkTranslation
   */
  protected ExternalLinkTranslation $externalLinkTranslation;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $tokenService;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['language']);
    $this->installConfig(['system']);
    $this->installEntitySchema('external_link_translation');

    $link = $this->entityTypeManager->getStorage('external_link_translation')->create([
      'link' => ['uri' => 'https://hidglobal.com/'],
    ]);
    $link->save();

    $this->externalLinkTranslation = $link;
    $this->tokenService = $this->container->get('token');
    $this->dateFormatter = $this->container->get('date.formatter');
    $this->languageManager = $this->container->get('language_manager');
  }

  /**
   * Data provider of tokens to test.
   */
  public static function externalLinkTokenProvider(): array {
    return [
      'Created token' => [
        '[external_link_translation:created]',
        'dynamic_create_date',
      ],
      'ID token'  => [
        '[external_link_translation:id]',
        '1',
      ],
      'Link token' => [
        '[external_link_translation:link]',
        'https://hidglobal.com/',
      ],
      'Changed token' => [
        '[external_link_translation:changed]',
        'dynamic_changed_date',
      ],
    ];
  }

  /**
   * Tests the tokens for an external link translation.
   *
   * @param string $token
   *   A token to test.
   * @param string $expected
   *   The expected replacement value.
   *
   * @dataProvider externalLinkTokenProvider
   */
  public function testExternalLinkTokenReplacement(string $token, string $expected) {
    $current_langcode = $this->languageManager->getCurrentLanguage()->getId();

    // Format date string if expected value is a dynamic date type.
    if (in_array($expected, ['dynamic_create_date', 'dynamic_changed_date'])) {
      $timestamp = $expected === 'dynamic_create_date'
        ? $this->externalLinkTranslation->getCreatedTime()
        : $this->externalLinkTranslation->getChangedTime();
      $expected = $this->dateFormatter->format($timestamp, 'medium', '', NULL, $current_langcode);
    }

    $bubbleable_metadata = new BubbleableMetadata();
    $output = $this->tokenService->replace($token, ['external_link_translation' => $this->externalLinkTranslation], ['langcode' => $current_langcode], $bubbleable_metadata);
    $this->assertSame($expected, $output, "Failed test case: {$token}");
  }

}
