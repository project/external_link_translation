<?php

namespace Drupal\Tests\external_link_translation\Unit\PathProcessor;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Tests\UnitTestCase;
use Drupal\external_link_translation\PathProcessor\ExternalLinkTranslationPathProcessor;
use PHPUnit\Framework\MockObject;

/**
 * Tests the external link translation path processor.
 *
 * @coversDefaultClass \Drupal\external_link_translation\PathProcessor\ExternalLinkTranslationPathProcessor
 * @group external_link_translation
 */
class ExternalLinkTranslationPathProcessorTest extends UnitTestCase {

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject
   */
  protected EntityTypeManagerInterface|MockObject $entityTypeManager;

  /**
   * The mocked language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface|\PHPUnit\Framework\MockObject
   */
  protected LanguageManagerInterface|MockObject $languageManager;

  /**
   * The mocked entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|\PHPUnit\Framework\MockObject
   */
  protected EntityStorageInterface|MockObject $entityStorage;

  /**
   * The tested path processor.
   *
   * @var \Drupal\external_link_translation\PathProcessor\ExternalLinkTranslationPathProcessor
   */
  protected ExternalLinkTranslationPathProcessor $pathProcessor;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->languageManager = $this->createMock('\Drupal\Core\Language\LanguageManagerInterface');
    $this->entityStorage = $this->createMock('\Drupal\Core\Entity\EntityStorageInterface');
    $this->pathProcessor = new ExternalLinkTranslationPathProcessor($this->entityTypeManager, $this->languageManager);

    $this->entityTypeManager->method('getStorage')
      ->with('external_link_translation')
      ->willReturn($this->entityStorage);
  }

  /**
   * Data provider of external links to test.
   */
  public static function externalLinkProvider(): array {
    return [
      'No path, fragment, or query' => [
        'url' => 'https://www.drupal.org/',
        'expected' => [
          'base_url' => 'https://www.drupal.org',
          'path' => '/',
          'fragment' => '',
          'query' => [],
        ],
      ],
      'No fragment or query' => [
        'url' => 'https://example.com/test',
        'expected' => [
          'base_url' => 'https://example.com',
          'path' => '/test',
          'fragment' => '',
          'query' => [],
        ],
      ],
      'No query' => [
        'url' => 'https://en.wikipedia.org/wiki/Mars_2020#Gallery',
        'expected' => [
          'base_url' => 'https://en.wikipedia.org',
          'path' => '/wiki/Mars_2020',
          'fragment' => 'Gallery',
          'query' => [],
        ],
      ],
      'With all parts' => [
        'url' => 'https://www.example.org/test?param=test#drupal',
        'expected' => [
          'base_url' => 'https://www.example.org',
          'path' => '/test',
          'fragment' => 'drupal',
          'query' => ['param' => 'test'],
        ],
      ],
      'With multiple query parameters' => [
        'url' => 'https://www.example.de/search?q=drupal&lang=en',
        'expected' => [
          'base_url' => 'https://www.example.de',
          'path' => '/search',
          'fragment' => '',
          'query' => ['q' => 'drupal', 'lang' => 'en'],
        ],
      ],
      'Complex URL with all components' => [
        'url' => 'http://docs.example.io/documentation/page.html?section=12&lang=en#title',
        'expected' => [
          'base_url' => 'http://docs.example.io',
          'path' => '/documentation/page.html',
          'fragment' => 'title',
          'query' => ['section' => '12', 'lang' => 'en'],
        ],
      ],
    ];
  }

  /**
   * Tests the path outbound processor with a non-matching path.
   */
  public function testOutboundWithNonMatchingPath() {
    $path = '/some/other/path';
    $options = [];

    $processed_path = $this->pathProcessor->processOutbound($path, $options);

    $this->assertEquals($path, $processed_path);
  }

  /**
   * Tests the path outbound processor with a non-matching route.
   */
  public function testOutboundWithNonMatchingRoute() {
    $path = '/some-other-route/123';
    $options = [
      'route' => $this->createMockRoute('/some-other-route/{some-other-route}'),
    ];

    $processed_path = $this->pathProcessor->processOutbound($path, $options);

    $this->assertEquals($path, $processed_path);
  }

  /**
   * Tests the path outbound processor with a matching route.
   *
   * This test represents the optimal case where the path processor correctly
   * identifies the matching route, loads the corresponding external translation
   * entity, and processes the path based on the entity's link data.
   *
   * @dataProvider externalLinkProvider
   */
  public function testOutboundWithMatchingRoute(string $url, array $expected) {
    $path = '/external-link-translation/1';
    $options = [
      'route' => $this->createMockRoute('/external-link-translation/{external_link_translation}'),
      'language' => $this->createMockLanguage(),
    ];

    // Create an external link translation entity.
    $id = '1';
    $elt = $this->createMock('\Drupal\external_link_translation\ExternalLinkTranslationInterface');
    $elt->method('getLink')
      ->willReturn($url);
    $this->entityStorage->method('load')
      ->with($id)
      ->willReturn($elt);

    $bubbleable_metadata = new BubbleableMetadata();
    $processed_path = $this->pathProcessor->processOutbound($path, $options, NULL, $bubbleable_metadata);
    $this->assertEquals($options['absolute'], TRUE);
    $this->assertEquals($options['prefix'], '');
    $this->assertEquals($options['base_url'], $expected['base_url']);
    $this->assertEquals($expected['path'], $processed_path);
    $this->assertEquals($options['fragment'], $expected['fragment']);
    $this->assertEquals($options['query'], $expected['query']);
    $this->assertContains('external_link_translation:1', $bubbleable_metadata->getCacheTags());
  }

  /**
   * Tests the path outbound processor with an invalid ID.
   *
   * Loading the entity should return null, resulting in no corresponding entity
   * being loaded and the original path being returned.
   */
  public function testOutboundWithInvalidId() {
    $path = '/external-link-translation/1';
    $options = [
      'route' => $this->createMockRoute('/external-link-translation/{external_link_translation}'),
      'language' => $this->createMockLanguage(),
    ];

    $processed_path = $this->pathProcessor->processOutbound($path, $options);
    $this->assertEquals($path, $processed_path);
  }

  /**
   * Tests the path outbound processor with an invalid link.
   *
   * The invalid link should fail the link validation check, resulting in the
   * original path being returned.
   */
  public function testOutboundWithInvalidLink() {
    $path = '/external-link-translation/1';
    $options = [
      'route' => $this->createMockRoute('/external-link-translation/{external_link_translation}'),
      'language' => $this->createMockLanguage(),
    ];

    // Create an external link translation entity.
    $id = '1';
    $elt = $this->createMock('\Drupal\external_link_translation\ExternalLinkTranslationInterface');
    $elt->method('getLink')
      ->willReturn('drupal.org/invalid-external-link');
    $this->entityStorage->method('load')
      ->with($id)
      ->willReturn($elt);

    $processed_path = $this->pathProcessor->processOutbound($path, $options);
    $this->assertEquals($path, $processed_path);
  }

  /**
   * Helper to create mock routes.
   */
  private function createMockRoute(string $path) {
    $route = $this->createMock('\Symfony\Component\Routing\Route');
    $route->method('getPath')->willReturn($path);
    return $route;
  }

  /**
   * Helper to create mock languages.
   */
  private function createMockLanguage(string $langcode = 'en') {
    $language = $this->createMock('\Drupal\Core\Language\LanguageInterface');
    $language->method('getId')->willReturn($langcode);
    return $language;
  }

}
